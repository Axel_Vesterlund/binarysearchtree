#pragma once
#include "stdafx.h"
template <typename T>
class LinkedList {
public:
	struct Node {
		T value;
		Node *next;
	};
	//template <typename T>
	LinkedList(){}
	~LinkedList() { Clear(); }
	template <typename T>
	void Push_Front(const T &value)
	{
		if (m_head == NULL)
		{
			Node *n = CreateNode(value);	
		}
		else
		{
			Node *n = CreateNode(value);

			Node* temp = CreateNode(0);;
			temp->next = m_head->next;
			m_head->next = n;
			n->next = temp->next;

			n->value = m_head->value;
			m_head->value = value;
		}
	}
	template <typename T>
	void Push_Back(const T &value)
	{
		if (m_head == NULL)
		{
			Node *n = CreateNode(value);
		}
		else
		{
			Node *n = CreateNode(value);

			Node *tmp;
			for (tmp = m_head; tmp->next; tmp = tmp->next);
			tmp->next = n;
		}


	}
	void Pop_Front()
	{
		if (m_head)
		{
			Node *tmp = m_head->next;
			m_head = tmp;
			tmp = NULL;
			delete tmp;	
		}

	}

	void Pop_Back()
	{
		if (m_head->next)
		{
			Node *tmp;
			for (tmp = m_head; tmp->next->next; tmp = tmp->next);
			tmp->next = NULL;
			delete tmp->next;
		}
		else
		{
			m_head = NULL;
			delete m_head;
		}


	}

	void Clear()
	{
		if(m_head)
		while (m_head->next)
		{
			Node *tmp;
			for (tmp = m_head; tmp->next->next; tmp = tmp->next);
			tmp->next = NULL;
			delete tmp->next;
		}
		m_head = NULL;
		delete m_head;
	}
	template <typename T>
	int Find(T Data)
	{
		Node *tmp;
		int i = 0;
		for (tmp = m_head; tmp->next; tmp = tmp->next)
		{
			i++;
			if (tmp->value == Data)
			{
				std::cout << "Value: " << Data << std::endl << "Found on: " << i << std::endl << std::endl;
				return i;
			}
		}
		return false;

	}
	unsigned int Size()
	{
		if (m_head && m_head->next)
		{
			unsigned int i = 1;
			Node *tmp;
			for (tmp = m_head; tmp->next; tmp = tmp->next)
			{
				i++;
			}
			return i;
		}
		else
		{
			if (m_head)
				return 1;
			else
				return 0;
		}

	}

private:
	

	template <typename T>
	Node* CreateNode(const T &value)
	{
		if (m_head == NULL)
		{
			m_head = new Node();
			m_head->value = value;
			m_head->next = NULL;
			return m_head;
		}
		else
		{
			Node *n = new Node();
			n->value = value;
			return n;
		}

	}

	Node *m_head;
};