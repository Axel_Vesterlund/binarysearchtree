#include "stdafx.h"
#include "LinkedList.h"

template <typename T>
struct Node
{
	T Value;
	Node* NextNode;
};
template <typename T>
LinkedList<T>::LinkedList()
{
	m_head = NULL;
}
template <typename T>
LinkedList<T>::~LinkedList()
{

}

template <typename T>
Node* LinkedList<T>::CreateNode(const T &value)
{
	if (m_head == NULL)
	{
		m_head = new Node();
		m_head->value = value;
		m_head->next = NULL;
		return m_head;
	}
	else
	{
		Node *n = new Node();
		n->value = value;
		m_head->next = NULL;
		return n;
	}

}

template <typename T>
void LinkedList<T>::Push_Front(const T &value)
{	
	Node *n = CreateNode(value);
}
template <typename T>
void LinkedList<T>::Push_Back(const T &value)
{
	Node *n = CreateNode(value);

	Node *tmp;
	for (tmp = m_head; tmp->next; tmp = tmp->next);
	tmp->next = n;
}
template <typename T>
T LinkedList<T>::Pop_Front()
{

}
template <typename T>
T LinkedList<T>::Pop_Back()
{

}
template <typename T>

//template <typename T>
void LinkedList<T>::Clear()
{

}
template <typename T>
T LinkedList<T>::Find(T Data)
{

}
template <typename T>
size_t LinkedList<T>::Size()
{
	
}

